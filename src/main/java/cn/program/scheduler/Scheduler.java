package cn.program.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Program on 2017/3/17.
 */
@Component
public class Scheduler {
    private static Logger logger = LoggerFactory.getLogger(Scheduler.class);
    private JedisCluster jedisCluster;

    /**
     * 为了测试方便，直接在这里初始化Redis
     */
    public Scheduler() {
        Set<HostAndPort> nodes = new HashSet<>();
        nodes.add(HostAndPort.parseString("192.168.1.100:7700"));
        nodes.add(HostAndPort.parseString("192.168.1.100:7000"));
        nodes.add(HostAndPort.parseString("192.168.1.100:7100"));
        this.jedisCluster = new JedisCluster(nodes);
    }

    /**
     * 1秒钟执行一次
     */
    @Scheduled(cron = "* * * * * ?")
    public void testScheduler() {
        String key = "lock-testScheduler";
        String ip = "192.168.1.100"; // 本机IP地址，方便测试这里随意写一个，实际情况，可以根据API获取

        // lock-testScheduler：key值，表明是锁的名字，后边跟上任务ID，这里随意分一个，实际情况按自己的来
        // 192.168.1.100：value值，表名当前占用锁的是哪一台机器
        // NX：表示当key不存在的时候设置
        // EX：锁的过期时间是10秒，按实际情况调，适当调大一些，该值一般要大于任务的执行时间
        // 如果设置成功，则返回字符串 OK ，设置失败则返回null
        String success = this.jedisCluster.set(key, ip, "NX", "EX", 10);

        // 拿到锁，拥有执行权限
        if ("OK".equals(success)) {

            try {
                logger.info("start execute...");

                // 其他操作....

            } finally { // 释放锁
                // 当前锁是否属于本机器
                String lockIp = this.jedisCluster.get(key);
                if (ip.equals(lockIp)) // 是自己锁的，那么就释放掉
                    this.jedisCluster.del(key);
            }

        } else { // 没拿到锁
            // 查看当前锁是被谁占用
            String lockIp = this.jedisCluster.get(key);
            logger.info("lock ip : {}", lockIp);
        }
    }

//    /**
//     * 1秒钟执行一次
//     */
//    @Scheduled(cron = "* * * * * ?")
//    public void testScheduler1() throws InterruptedException {
//        logger.info("testScheduler-1...");
//        Thread.sleep(2000);
//    }
}
