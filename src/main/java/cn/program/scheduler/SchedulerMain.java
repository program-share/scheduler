package cn.program.scheduler;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Program on 2017/3/17.
 */
public class SchedulerMain {

    public static void main(String[] args) throws InterruptedException {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicatin.xml");
        Thread.currentThread().join();
    }

}
